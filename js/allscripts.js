$(function() {
    $('.bxslider').bxSlider({
        pager: false,
        auto: true
    });

    $("#slider2").responsiveSlides({
        manualControls: '#slider2-pager',
        auto: false
    });

    $("a.contact-to").click(function() {
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top + "px"
        }, {
            duration: 900,
            easing: "swing"
        });
        return false;
    });


});